const objectPath = require('object-path')
const locale = require('../../src/locales/en.default.json')

module.exports = function (engine) {
  engine.registerFilter('t', async (key, value) => {
    let translation = objectPath.get(locale, key) || key;
    let data = {}
    if (typeof value !== 'undefined') {
      data[value[0]] = value[1]
    }
    return await engine.parseAndRender(translation, data);
  })
}
