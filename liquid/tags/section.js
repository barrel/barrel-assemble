module.exports = function (liquid) {
  liquid.registerTag('section', {
    parse: function (tagToken, remainTokens) {
      const staticFileRE = /[^\s,]+/;
      let match = staticFileRE.exec(tagToken.args);
      if (match)
          this.staticValue = match[0];
      this.moduleName = this.staticValue.slice(1, -1)
      this.templates = liquid.getTemplateSync(`./../../src/sections/${this.moduleName}.liquid`)
    },
    render: async function (ctx, hash, emitter) {
      let moduleData = require(`./../../src/modules/${this.moduleName}/section.json`)
      return await liquid.render(this.templates, moduleData, emitter);
    }
  })
}
