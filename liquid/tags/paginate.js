module.exports = function (liquid) {
  liquid.registerTag('paginate', {
    parse: function (tagToken, remainTokens) {
      this.templates = []

      var stream = liquid.parser.parseStream(remainTokens)
      stream.on('tag:endpaginate', token => stream.stop())
        .on('template', tpl => this.templates.push(tpl))
        .on('end', x => {
          throw new Error(`tag ${tagToken.raw} not closed`)
        })
      stream.start()
    },
    render: function (scope, hash) {
      return liquid.renderer.renderTemplates(this.templates, scope)
    }
  })
}
