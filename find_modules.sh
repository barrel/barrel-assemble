#!/bin/bash

# turn off field splitting during command substitution
#IFS=$'\n'
GLOB="src/**/**/*.liquid"
MODULES=$(grep -h 'include' $GLOB | sed -E "s/(.*include \')([a-zA-Z\-]+)\'(.*)/\2/" | sort | uniq)
ERRORS=()
EXISTS=0
COPIED=0
echo "$MODULES" | while read -r MODULE ; do
  if [ ! -d "./src/modules/$MODULE" ]; then
    cp -r ../barrel-assemble/src/modules/$MODULE ./src/modules/ 2>/dev/null
    if [[ "$?" -ne 0 ]]; then
      ERRORS+=( "$MODULE" )
    else
      COPIED=$(($COPIED+1))
    fi
  else
    EXISTS=$(($EXISTS+1))
  fi
done
echo "Modules Found: $EXISTS"
echo "Modules Copied: $COPIED"
echo "Modules Not Found:"
printf '%s\n' "${ERRORS[@]}"
