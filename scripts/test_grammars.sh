#!/bin/bash

####################################################################
## This script is responsible for performing linting and syntax
## checks on js, css, and php files.
##
## - Liquid: uses custom linting rules and checks all liquid files
## - CSS: uses stylelint with stylelint-config-standard config
## - JS : uses standardjs for code style enforcement
## - ECL: uses editorconfig to check that files match the rules
## - BUILD: uses node to install and run theme build process
##
## Note: Tests can be run locally by running `npm test` command. 
####################################################################

ERRORS=0
ROOT_PATH=$(git rev-parse --show-toplevel)
SCRIPT_PATH="`dirname \"$0\"`"

# Terminal colors
source $SCRIPT_PATH/colors.sh
cd $ROOT_PATH

echo "${YELLOW}Performing JSON syntax check...${DEFAULT}"
npm run test:json_lint
if [[ "$?" -ne 0 ]]; then
    echo "${RED}JSON syntax check failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Installing theme dependencies...${DEFAULT}"
npm ci
if [[ "$?" -ne 0 ]]; then
    echo "${RED}Dependency installation failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Testing js against eslint...${DEFAULT}"
npm run test:js_lint
if [[ "$?" -ne 0 ]]; then
    echo "${RED}Conformance to eslint failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Testing css against stylelint...${DEFAULT}"
npm run test:css_lint
if [[ "$?" -ne 0 ]]; then
    echo "${RED}Conformance to stylelint failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Testing files against liquid linter...${DEFAULT}"
npm run test:liquid_lint
if [[ "$?" -ne 0 ]]; then
    echo "${RED}Conformance to liquid linter failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Testing files against editorconfig...${DEFAULT}"
npm run test:editorconfig
if [[ "$?" -ne 0 ]]; then
    echo "${RED}Conformance to editorconfig failed!${DEFAULT}"
    ERRORS=$(($ERRORS+1))
fi
echo $OK

echo "${YELLOW}Tallying sum of failures...${DEFAULT}"
if [[ "$ERRORS" -gt "0" ]]; then 
    echo "${RED}There were $ERRORS errors encountered! Please review the errors above.${DEFAULT}"
    exit $ERRORS
fi

echo "${GREEN}Grammar and sanity checks complete!${DEFAULT}"
exit 0