#!/usr/bin/env bash

export ENV=staging;
export STAGE=backup;

#export SHOPIFY_API_KEY=;
#export SHOPIFY_PASSWORD=;
#export SHOPIFY_STORE=shop-laws-of-motion.myshopify.com;

# Here, we make sure we have a local development
# branch that we can use when we make decisions
# based on the git history and branch
git fetch --all;
git checkout master;
git reset --hard origin/master;
git checkout develop;
git reset --hard origin/develop;
git checkout "$CI_COMMIT_REF_NAME";

# Pull in node modules
npm i;

# Run our npm managed deployment script
node ./node_modules/.bin/brrl-deploy;
