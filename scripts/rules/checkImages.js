module.exports = {
  id: 'IMAGES',
  regex: /<img/,
  filter (fileName) {
    return true
  },
  errors (lines) {
    return lines.filter(({file}) => {
      return !~file.indexOf('image')
    })
  },
  message (line) {
    return 'Image rendered without image module.'
  },
  neededMatches: 1
}
