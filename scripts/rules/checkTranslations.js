const objectPath = require('object-path')
const locale = require('../../src/locales/en.default.json')

module.exports = {
  id: 'TRANSLATIONS',
  regex: / ['"](.[^'"]*)['"] ?\| ?t/,
  filter (fileName) {
    return true
  },
  errors (lines) {
    return lines
      .filter(({matches}) => {
        if (objectPath.get(locale, matches[1])) {
          return false
        }
        return true
      })
  },
  message ({matches}) {
    return `"${matches[1]}" not found in translation file.`
  },
  neededMatches: 2
}
