module.exports = {
  id: 'CAMELCASE',
  regex: /{%-? +(?:capture|assign) +([a-z]*[a-z][A-Z][a-z]*)/,
  filter (fileName) {
    return true
  },
  errors (lines) {
    return lines
  },
  message () {
    return 'Variable assignment should not be in camel case'
  },
  neededMatches: 1
}
