/**
 * @author Barrelny.com
 * @updated 3/31/2019
 *
 * This script looks into all liquid snippets and checks
 * to see if all assigned variables have been nullified.
 *
 * Skips templates, sections and theme files.
 * Ignores variables prefixes with 'global_'
 */
const glob = require('glob-all')
const fs = require('fs')
const colors = require('colors/safe')
const rules = [
  ...glob.sync(['./scripts/rules/*.js']).map(fileName => {
    return require('./' + fileName.split('/scripts/')[1])
  })
]

const config = require(`${process.cwd()}/.liquidrc.js`)

const files = glob.sync(config.files || 'src/**/*.liquid')

function deepClone (obj) {
  return JSON.parse(JSON.stringify(obj))
}

const TESTING = process.env.TESTING || false

/**
 * Takes the file array returned from the glob call
 * above and turns it into a promise chain. Each link
 * in the chain resolves after all rules have been
 * run on that specific file.
 */
Promise.all(files.map(file => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf8', (err, contents) => {
      if (err) {
        reject(file)
      }
      resolve(
        runLintingRules(contents, file)
      )
    })
  })
}))

/**
 * After all of the rules have been run,
 * we concat all of the results and present
 * them to stdin.
 */
.then(allResults => {
  if (TESTING) {
    process.exit()
  }

  const combined = allResults.reduce((arr, result) => {
    if (!result) {
      return arr
    }
    return arr.concat(result)
  }, [])

  const fileMappedMessages = combined
  .reduce((obj, {file, matches, line, error}) => {
    if (typeof obj[file] === 'undefined') {
      obj[file] = []
    }
    obj[file].push({
      line,
      message: `${colors.green('Line ' + line)} ${error}`
    })
    return obj
  }, {})

  const keys = Object.keys(fileMappedMessages)
  keys.sort((a, b) => {
    const fileNameA = a.split('/').pop()
    const fileNameB = b.split('/').pop()
    if (fileNameA < fileNameB) return -1
    if (fileNameA > fileNameB) return 1
    return 0
  })
  keys.forEach(file => {
    console.log(colors.red.underline(file))

    fileMappedMessages[file].sort((a, b) => {
      const lineA = Number(a.line)
      const lineB = Number(b.line)
      if (lineA < lineB) return -1
      if (lineA > lineB) return 1
      return 0
    })

    console.log(fileMappedMessages[file].map(({message}) => message).join('\n'))
    console.log('\n')
  })

  if (combined.length) {
    console.log(`--`)
    console.log(`${colors.rainbow('Summary')}`)
    console.log(`${colors.white.underline(`${combined.length} errors found`)}`)
    console.log('\n')
    process.exit(1)
  } else {
    console.log(`${colors.rainbow('No Errors Found!')}`)
    process.exit(0)
  }
})

/**
 * This is called for every file returned from the
 * glob at the start of this file. Here, we take the
 * contents of each file, split it via new line character
 * and run each of the linting rules to each line.
 *
 * @param {String} contents Contents of the file
 * @param {String} fileName The file name
 */
function runLintingRules (contents, fileName) {
  const lines = contents
  .split('\n')
  .map((content, index) => {
    return {
      content,
      line: index,
      file: fileName
    }
  })
  .filter(({content}) => !!content)
  .filter(({content}) => {
    return !~content.indexOf('lint:ignore')
  })

  const resultsOfAllRules = rules
    .filter(({id}) => {
      if (typeof config.rules[id] === 'undefined') {
        return true
      }
      return !!Number(config.rules[id])
    })
    .reduce((arr, rule) => {
      arr = arr.concat(
        runLintingRule(rule, deepClone(lines), fileName)
      )

      return arr
    }, [])

  return resultsOfAllRules
}

/**
 * This is called for every rule, on every file.
 * Here, we filter out lines via the rule's regex
 * and then run the rule.errors method on the line
 * to see if we should return an error message.
 *
 * @param {Object} rule The rule (./rules/*.js) to run
 * @param {Array} lines The lines to run the rule on
 * @param {String} fileName The file name currently under inspection
 */
function runLintingRule (rule, lines, fileName) {
  if (!rule.filter(fileName)) {
    return []
  }

  const matches = lines.reduce((arr, line) => {
    if (line.content.match(rule.regex)) {
      arr = arr.concat({
        ...line,
        matches: line.content.match(rule.regex)
      })
    }
    return arr
  }, []).filter(line => {
    if (!line.matches.length) {
      return false
    }
    if (typeof line.matches[(rule.neededMatches || 1) - 1] === 'undefined') {
      return false
    }
    if (!line.matches[(rule.neededMatches || 1) - 1].trim()) {
      return false
    }
    return true
  })

  if (!matches.length) {
    return []
  }

  return rule.errors(matches).map(line => {
    return {
      ...line,
      error: rule.message(line)
    }
  })
}

