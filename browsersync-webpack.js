/**
 * Require Browsersync along with webpack and middleware for it
 */
var browserSync = require('browser-sync').create();
var webpack = require('webpack');
var WDM = require('webpack-dev-middleware');
var WHM = require('webpack-hot-middleware')

/**
 * Require ./webpack.config.js and make a bundler from it
 */
var webpackConfig = require('./webpack.config');
var bundler = webpack(webpackConfig);

/**
 * Run Browsersync and use middleware for Hot Module Replacement
 */
browserSync.init({
  proxy: {
    middleware: [
      WDM(bundler, {
        publicPath: webpackConfig.output.publicPath,
        noInfo: true,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }),
      WHM(bundler)
    ],
    proxyReq: [
      ( proxyReq ) => {
        proxyReq.setHeader('X-DEV', '1')
      }
    ],
    target: 'https://barrel-assemble.lndo.site/',
  },
  open: 'internal',
  host: 'localhost',
  https: {
    key: `${__dirname}/server/cert/s.key`,
    cert: `${__dirname}/server/cert/s.crt`
  },
  notify: false
});
