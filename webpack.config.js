const path = require('path')
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const isDevMode = process.env.NODE_ENV === 'development'
const config = {
  publicPath: isDevMode ? '/dev/' : '/assets/',
  package: require('./package.json'),
  local: 'localhost',
  entry: {
    main: [
      './src/css/main.css',
      './src/js/main'
    ]
  },
  CSSLoaders: [
    'style-loader',
    'css-loader?importLoaders=1&minimize=1',
    'postcss-loader'
  ]
}
if (isDevMode) {
  config.entry.main.push('webpack-hot-middleware/client?reload=true')
} else {
  config.CSSLoaders.splice(1, 0, {
    loader: MiniCssExtractPlugin.loader,
  })
}
console.log('isDevMode='+isDevMode)

module.exports = {
  mode: isDevMode ? 'development' : 'production',
  devtool: 'cheap-module-source-map',
  entry: {
    main: config.entry.main
  },
  output: {
    path: path.join(__dirname, 'dist/assets'),
    filename: isDevMode ? '[name].js' : '[name].min.js',
    chunkFilename: `[name]-[id].js?version=${Date.now()}`,
    publicPath: config.publicPath
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader?emitError=true'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.s?css$/,
        use: config.CSSLoaders
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=/fonts/[name].[ext]'
      }
    ]
  },
  resolve: {
    alias: {
      'lib': path.resolve(__dirname, 'src/js/lib'),
      'mixins': path.resolve(__dirname, 'src/js/mixins'),
      'modules': path.resolve(__dirname, 'src/modules'),
      'vue': !isDevMode ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js',
      'root': path.resolve(__dirname, 'src')
    }
  },
  plugins: [
    ...(isDevMode ? [
      new webpack.SourceMapDevToolPlugin(),
      new webpack.HotModuleReplacementPlugin(),
    ] : [
      new MiniCssExtractPlugin({
        filename: '[name].min.css',
        //chunkFilename: '[id].css',
        ignoreOrder: false, // Enable to remove warnings about conflicting order
      }),
    ]),
    new StyleLintPlugin({
        files: [
          'src/assets/css/*.css',
          'src/modules/**/*.css'
        ]
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      DEBUG: !(process.env.NODE_ENV === 'production'),
      BRRL_PUBLIC_PATH: JSON.stringify(config.publicPath),
      BRRL_VERSION: JSON.stringify(config.package.version),
      BRRL_PROXY: JSON.stringify(config.local),
      BRRL_PATH: function(path, proxy) {
        if (!proxy) {
          proxy = 'localhost';
        };
        if (document.location.hostname === proxy) {
          return path;
        } else {
          return SHOPIFY_CDN;
        };
      }
    })
  ]
}
