import init from 'lib/init'

document.addEventListener('DOMContentLoaded', () => {
  init().mount()
})
