import select from 'select-dom'
import inViewport from 'in-viewport'
import {
  set,
  isOver,
  isFirefox
} from 'lib/util'

const breakpoint = 768

export default (el) => {
  let video = select('.js-video', el) || false

  if (video && isOver(breakpoint)) {
    let src = video.getAttribute('data-src')

    inViewport(video, {
      offset: 300
    }, () => {
      if (isFirefox()) {
        video.setAttribute('src', src)
        set(el, 'is-loaded')
      } else {
        fetch(src)
          .then(() => {
            video.setAttribute('src', src)
          })
          .then(() => {
            set(el, 'is-loaded')
          })
      }
    })
  }
}
