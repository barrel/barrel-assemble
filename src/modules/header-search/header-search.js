import Vue from 'vue'
import vueQuickSearch from 'vue-quick-search'
import state from 'lib/appState'

export default el => new Vue({
  el,
  components: {
    'vue-quick-search': vueQuickSearch
  },
  data () {
    return {
      state
    }
  },
  methods: {
    getResults (url, next) {
      next(200, {
        results: [{
          url: 'http://www.docwinebar.com/',
          title: 'DOC Winebar',
          image: 'https://cdn.shopify.com/s/files/1/0022/6816/8257/products/001_56d55d12-8881-47aa-8fda-0de9d121dfc4_1080x.jpg?v=1537539891'
        },
        {
          url: 'http://www.docwinebar.com/',
          title: 'DOC Restaurant',
          image: 'https://static1.squarespace.com/static/5388d91ae4b0813d34318751/t/54270232e4b08dba836b4f6e/1411842612762/IMG_4431.jpg'
        },
        {
          url: 'http://www.docwinebar.com/',
          title: 'DOC Beer Hall',
          image: 'https://cdn.shopify.com/s/files/1/0022/6816/8257/products/014_bbc9114b-7f56-442e-8b8e-38d84c46943e_1080x.jpg?v=1539116941'
        },
        {
          url: 'http://www.docwinebar.com/',
          title: 'DOC Winebar',
          image: 'https://static1.squarespace.com/static/5388d91ae4b0813d34318751/t/54270232e4b08dba836b4f6e/1411842612762/IMG_4431.jpg'
        },
        {
          url: 'http://www.docwinebar.com/',
          title: 'DOC Winebar',
          image: 'https://cdn.shopify.com/s/files/1/0022/6816/8257/products/001_502578ce-1965-4ccd-ae3c-f908dc486cb3_1080x.jpg?v=1539117675'
        }],
        total: 5
      })
    },
    hideSearch () {}
  },
  template: `
    <vue-quick-search
      :is-active="state.isSearchOpen"
      :get-results="getResults"
      :result-limit="4"
      @hide="hideSearch">
      <template slot="no-results" slot-scope="props">
        <div>No results available for {{ props.searchTerm }}</div>
      </template>
    </vue-quick-search>
  `
})
