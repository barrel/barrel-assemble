import Flickity from 'flickity'

export default el => {
  const instance = new Flickity(el, {
    autoPlay: 5000,
    prevNextButtons: false,
    wrapAround: true
  })

  el.classList.add('is-init')

  return instance
}
