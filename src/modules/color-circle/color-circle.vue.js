import Vue from 'vue'
import {
  handleize
} from 'lib/util'

const qs = Math.ceil(Date.now() / 60 / 60 / 24) // Changes once a day
const URL = 'https://cdn.shopify.com/s/files/1/2403/8187/files/2018-06-25-'

Vue.component('color-circle', {
  props: {
    isActive: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: ''
    },
    type: {
      type: String,
      default: 'image'
    }
  },
  data () {
    return {
      imageError: false
    }
  },
  computed: {
    imageSrc () {
      return `${URL}${handleize(this.title)}.jpg?${qs}`
    },
    styles () {
      if (this.type === 'image') {
        return {
          'backgroundImage': `url(${this.imageSrc})`
        }
      }
      return {}
    },
    classes () {
      if (this.type === 'css') {
        return `bg-${handleize(this.title)}`
      }
    }
  },
  methods: {
    showImageErrorState () {
      this.imageError = true
    }
  },
  template: `
    <div
      :title="title"
      class="color-circle"
      @click="$emit('click')"
      :class="[{'no-image': imageError, 'is-active': isActive}, classes]"
      :style="styles">
    </div>
  `
})
